# WorkflowBundle

This bundle allows to define editorial workflows and wiring those to ez content types identifier(s).

## What it does

It introduces Symfony's workflow component, allowing to define workflows via yml and wiring each workflow definition to one or multiple ez content types.

http://symfony.com/doc/current/components/workflow.html

The initial matching data in ez storage needs to be handled on project level by custom migrations or similar! 

## Technical adaptions to basic symfony configuration

- we are more or less copying the DI configuration code from SymfonyFrameworkBundle (v3.2) into the bundle (needs to be kept in sync until ez is allowing to upgrade to 3.2)
- we are introducing our onw Registry class, inspired by the default one (but no interface yet?) from symfony's workflow component with a custom one using CompilerPass to change workflow.registry service tou ours.
- configuration happens in the same way to how it is documented for symfony component with following adaptions:
    - definition is not done under "framework > workflows" but under "ez_workflow > workflows"
    - for "marking_store", we need to define define service "ez_workflow.marking_store.object_state_storage" provided by this bundle
    - for "supports" key, we do not provide class names but content type identifier strings of ez content managed in current ez instance 
- be aware that we are not supporting content being in multiple "places" at once!

### Example configuration for workflow, it's states ("places") and transitions

```
ez_workflow:
    workflows:
        editorial_workflow:
            type: 'state_machine'
            marking_store:
                service: ez_workflow.marking_store.object_state_storage # use custom marking store service
            supports:
                - article # register for ez content of content type identifier "article" only
            places: [ draft, prepared, published, unpublished ]
            transitions:
                finish:
                    from: draft
                    to: prepared
                reject:
                    from: prepared
                    to: draft
                approve:
                    from: prepared
                    to: published
                force_publish:
                    from: draft
                    to: published
                unpublish:
                    from: published
                    to: unpublished
                recover:
                    from: unpublished
                    to: prepared
```

## Extentions

###  Storage

We are wiring ez's object state storage as new custom marking store to workflow component. 

Note: For that, inside the project, object state group and object states below it need to be added to ez storage, e.g. via kaliop migrations (with names being the same as in yml workflow configuration!).
Changes to available states and workflows need to be manually adapted as well, be careful to remove a state that still could be attached to an ez content object in your project!

Be aware: At the moment, introducing a custom object state groups leads to ez storage adding a reference to the first (as considered "initial") state of this group to *any* content in ez storage, not only the content with relevant content type... 

#### Example kaliop migration for object states and object state group

```
    -
        mode: create
        type: object_state_group
        identifier: 'editorial_workflow' # name of configured workflow, see example above
        names:
            eng-GB: Editorial workflow
        descriptions:
            eng-GB: Editorial workflow
        references:
            -
              attribute: id
              identifier: object_state_group_editorial_workflow
    -
        mode: create
        type: object_state
        object_state_group: reference:object_state_group_editorial_workflow
        identifier: 'draft' # name of configured state, see example above
        names:
            eng-GB: Draft
        descriptions:
            eng-GB: Content is in draft state
    -
        ... # create object_state objects with same object_state_group reference as for "draft" for any other state
```

### Permissions

#### Frontend / read permissions 

To restrict access to content depending on it's content state, the anonymous user 's permissions should be adjusted.

##### Example kaliop migration for user permission policy

```
    -
        mode: update
        type: role
        name: Anonymous
        policies:
            -
                ... # any other policies
            -
                module: content
                function: read
                limitations:
                    -
                        identifier: Class
                        values:
                            - 'article' # identifier of content type(s) under workflow
                    -
                        identifier: State
                        values: [5] # id's of ez storage object state, e.g. of "published" state
            -
                module: content
                function: read
                limitations:
                    -
                        identifier: Class
                        values:
                            - 'folder'
                            - ... # any other content type identifiers the user should be able to read that are *not* under workflow
```


#### Admin / write permissions

By default we are wiring existing ez permissions into the symfony workflow component. 

We introduced our own GuardListener service that decides which user can see and trigger a transition, by checking for user having permissions to *both* of the following: 
1) "content:edit" permission on the current content object (you can add policy limitations like "State" or similar) 
2) "state:assign" for current content object to that new state name (using ez's "NewState" policy limitation).

These two checks are implemented in src/EzBridge/GuardListener.php, we might want to add an abstraction layer here by moving it out into an interface.

##### Example kaliop migration for user permission policy
 
```
    -
        mode: update
        type: role
        name: Journalist
        policies:
            -
                module: content
                function: edit
                limitations:
                    -
                        identifier: Class
                        values:
                            - 'article'
                    -
                        identifier: State
                        values:
                            - 3 # object state id for "draft" state
            -
                module: state
                function: assign
                limitations:
                    -
                        identifier: Class
                        values:
                            - 'article'
                    -
                        identifier: NewState
                        values:
                            - 4 # object state id for "prepared" state
 
``` 

### "REST" API

For the API to be accessible (what is needed to provide out of the box integration into platform UI, see below), you need to register the bundle's controller as a routing source:

```
    workflow:
        resource: "@InviqaEzWorkflowBundle/Controller/"
        type: annotation
        prefix: /ez-workflow
```

It will register following routes / RESTish endpoints:

1) Is content type assigned to a workflow?
 
Under <bundle-prefix>/supported/{contentTypeIdentifier}, as an authenticated user (else 401), you will retrieve a JSON response like:

```
    {
        'has_workflow': bool # true if it is, false else
    }
```

2) Available transitions

Under <bundle-prefix>/transitions/{contentId}, as an authenticated user (else 401), you will retrieve a JSON response like:

```
    {
        'has_workflow': bool, # same result as for request above
        'workflow': string, # Name of the workflow
        'state': string, # "Labelled" name (some ucfirst and similar) of current state identifier
        'transitions': {
            string: string, # map of transition identifier (e.g. "force_publish") to it's "labelled" string representation (can be used to display button text) (e.g. "Force publish") 
            string: string,  
        }
    }
```

3) Trigger transition

Under <bundle-prefix>/transitions/{contentId}/{transitionName}, as an authenticated user (else 401), you will retrieve a JSON response like:

```
    {
        'success': bool
    }
```

- If transition was successful, the return value will be true with http status code 204

- In any other case the return value is false with http status 400 (this might need some refactoring to allow more specific responses)


### Platform UI integration

The bundle comes with an easy approach to introduce buttons to right menu when in "view" mode of a content in eZ's admin UI.

For that, following adjustments will be done to ez's content "view" mode via yui:
- REST endpoint will be accessed to check if content is "under workflow":
    - if it is not, no further changes to ui will be done
    - if it is:
        - the "save" (as draft) button will be hidden (as object state handling does not work with drafts in eZ)
        - the "publish" button will be renamed to "save changes" and *any* click on that will create a new published (in ez context) version of content
        - one "disabled" button will be added to the right actions menu displaying the current state of content item
        - for every available transition of that content, additional buttons will be added right below the state indicating button
 
Be aware that currently, the bundle's routing prefix needs to be set to "/ez-workflow" as it's hardcoded in js files!


## Maintenance / Roadmap

### Symfony workflow dependency

New concept of "SupportStrategy" will be released with Symfony 3.3. It will allow to revert our custom WorkflowRegistry and use that approach instead to identify which workflow is assigned to what ez content.

Even then, as ez is not supporting Symfony 3.x for now, we are forced to integrate workflow component in a custom way, adapting 3.2's FrameworkBundle approach.
