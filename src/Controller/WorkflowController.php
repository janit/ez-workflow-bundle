<?php

namespace Inviqa\EzWorkflowBundle\Controller;

use eZ\Publish\API\Repository\Values\Content\Content;
use Inviqa\EzWorkflowBundle\EzBridge\WorkflowNotFoundException;
use Inviqa\EzWorkflowBundle\EzBridge\WorkflowRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Authorization\Voter\AuthenticatedVoter;
use Symfony\Component\Workflow\Exception\LogicException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * @Route(service="ez_workflow.controller")
 */
class WorkflowController extends Controller
{
    /** @var \Inviqa\EzWorkflowBundle\EzBridge\WorkflowRegistry */
    private $workflowRegistry;

    /** @var \Symfony\Component\Security\Core\Authorization\AuthorizationChecker */
    private $authorizationChecker;

    /**
     * @param \Inviqa\EzWorkflowBundle\EzBridge\WorkflowRegistry $workflowRegistry
     * @param \Symfony\Component\Security\Core\Authorization\AuthorizationChecker $authorizationChecker
     */
    public function __construct(WorkflowRegistry $workflowRegistry, AuthorizationChecker $authorizationChecker)
    {
        $this->workflowRegistry = $workflowRegistry;
        $this->authorizationChecker = $authorizationChecker;
    }

    /**
     * @Route("/transitions/{contentId}", name="workflow_available_transitions", requirements={"contentId"="\d+"})
     *
     * @param \eZ\Publish\API\Repository\Values\Content\Content $content
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws AccessDeniedHttpException
     */
    public function getAvailableTransitionsAction(Content $content)
    {
        if (!$this->authorizationChecker->isGranted(AuthenticatedVoter::IS_AUTHENTICATED_FULLY)) {
            throw new AccessDeniedHttpException();
        }

        try {
            $workflow = $this->workflowRegistry->get($content);
        } catch (WorkflowNotFoundException $ex) {
            return $this->createAvailableTransitionsResponse(null, null, []);
        }

        try {
            $marking = $workflow->getMarking($content);
        } catch (LogicException $ex) {
            return $this->createAvailableTransitionsResponse($workflow->getName(), 'error', []); // we do not want to fallback to default ez behavior!
        }

        $currentStateName = key($marking->getPlaces());
        $transitions = $workflow->getEnabledTransitions($content);
        $availableTransitions = [];
        foreach ($transitions as $transition) {
            $availableTransitions[] = $transition->getName();
        }

        return $this->createAvailableTransitionsResponse($workflow->getName(), $currentStateName, $availableTransitions);
    }

    /**
     * @Route("/supported/{checkedContentType}", name="workflow_supported_check")
     *
     * @param string $checkedContentType
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws AccessDeniedHttpException
     */
    public function hasSupportedWorkflowCheckAction($checkedContentType)
    {
        if (!$this->authorizationChecker->isGranted(AuthenticatedVoter::IS_AUTHENTICATED_FULLY)) {
            throw new AccessDeniedHttpException();
        }

        $hasSupportedWorkflow = $this->workflowRegistry->has($checkedContentType);

        return $this->createWorkflowSupportResponse($hasSupportedWorkflow);
    }

    /**
     * @Route("/transitions/{contentId}/{transitionName}", name="workflow_apply_transition", requirements={"contentId"="\d+"}, methods="POST")
     *
     * @param \eZ\Publish\API\Repository\Values\Content\Content $content
     * @param string $transitionName
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws AccessDeniedHttpException
     */
    public function applyTransitionAction(Content $content, $transitionName)
    {
        if (!$this->authorizationChecker->isGranted(AuthenticatedVoter::IS_AUTHENTICATED_FULLY)) {
            throw new AccessDeniedHttpException();
        }

        try {
            $workflow = $this->workflowRegistry->get($content);
        } catch (WorkflowNotFoundException $ex) {
            return $this->createTransitionResponse(false);
        }

        try {
            $workflow->apply($content, $transitionName);
        } catch (LogicException $ex) {
            return $this->createTransitionResponse(false);
        }

        return $this->createTransitionResponse(true);
    }

    /**
     * @param string|null $workflowName
     * @param string|null $currentStateName
     * @param array|string[] $availableTransitions
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    private function createAvailableTransitionsResponse($workflowName = null, $currentStateName = null, array $availableTransitions)
    {
        $statusCode = 200;
        if ($currentStateName === 'error') {
            $statusCode = 501;
        }

        if (null !== $availableTransitions) {
            $transitions = [];
            foreach ($availableTransitions as $transitionName) {
                $transitions[$transitionName] = $this->convertTechStringIntoLabel($transitionName);
            }
        } else {
            $transitions = null;
        }

        return new JsonResponse([
            'has_workflow' => ($workflowName !== null),
            'workflow' => $workflowName,
            'state' => $currentStateName ? $this->convertTechStringIntoLabel($currentStateName) : null,
            'transitions' => $transitions
        ], $statusCode);
    }

    /**
     * @param bool $success
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    private function createTransitionResponse($success)
    {
        $statusCode = 204;
        if (!$success) {
            $statusCode = 400;
        }

        return new JsonResponse([
            'success' => $success,
        ], $statusCode);
    }

    /**
     * @param bool $isSupported
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    private function createWorkflowSupportResponse($isSupported)
    {
        return new JsonResponse([
            'has_workflow' => $isSupported,
        ], 200);
    }

    /**
     * @param string $techString
     * @return string
     */
    private function convertTechStringIntoLabel($techString)
    {
        return ucwords(str_replace('_', ' ', $techString));
    }
}

