<?php

namespace Inviqa\EzWorkflowBundle;

use Inviqa\EzWorkflowBundle\DependencyInjection\EzWorkflowExtension;
use Inviqa\EzWorkflowBundle\DependencyInjection\ChangeRegistryPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class InviqaEzWorkflowBundle extends Bundle
{
    /**
     * @{inheritdoc}
     */
    public function build(ContainerBuilder $container)
    {
        $container->addCompilerPass(new ChangeRegistryPass());
    }

    /**
     * @{inheritdoc}
     */
    public function getContainerExtension()
    {
        return new EzWorkflowExtension();
    }
}
