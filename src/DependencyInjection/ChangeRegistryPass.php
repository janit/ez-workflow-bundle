<?php

namespace Inviqa\EzWorkflowBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class ChangeRegistryPass implements CompilerPassInterface
{
    /**
     * @{inheritdoc}
     */
    public function process(ContainerBuilder $container)
    {
        $container->findDefinition('workflow.registry')->setClass('Inviqa\EzWorkflowBundle\EzBridge\WorkflowRegistry')
            ->setArguments([new Reference('ezpublish.api.service.content_type')]);
    }
}