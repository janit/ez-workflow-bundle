<?php

namespace Inviqa\EzWorkflowBundle\EzBridge;

use eZ\Publish\API\Repository\Values\Content\Content;
use eZ\Publish\API\Repository\ContentTypeService;
use Symfony\Component\Workflow\Exception\InvalidArgumentException;
use Symfony\Component\Workflow\Workflow;

/**
 * Initially based on a copy of Symfony\Component\Workflow\Registry while changing it's behavior from mapping a class name to a workflow to a ez content's content type.
 * Further changes had to be made to introduce new public method has() to initially check, if to be created content of a content type will have a workflow attached.
 */
class WorkflowRegistry
{
    /** @var array|array[] Array with two elements, one is a Workflow instance, one a registered ez content type identifier for that workflow */
    private $workflows = array();

    /** @var \eZ\Publish\API\Repository\ContentTypeService */
    private $contentTypeService;

    /**
     * @param \eZ\Publish\API\Repository\ContentTypeService $contentTypeService
     */
    public function __construct(ContentTypeService $contentTypeService)
    {
        $this->contentTypeService = $contentTypeService;
    }

    /**
     * @param Workflow $workflow
     * @param string   $contentType
     */
    public function add(Workflow $workflow, $contentType)
    {
        $this->workflows[] = array($workflow, $contentType);
    }

    /**
     * @param Content $subject
     * @param null|string $preferredWorkflowName
     *
     * @return Workflow
     * @throws WorkflowNotFoundException If no workflow found
     * @throws InvalidArgumentException If more than one workflow was found for a content
     */
    public function get(Content $subject, $preferredWorkflowName = null)
    {
        $matched = null;

        foreach ($this->workflows as list($workflow, $supportedContentType)) {
            if ($this->supports($workflow, $supportedContentType, $this->getContentTypeIdentifier($subject), $preferredWorkflowName)) {
                if ($matched) {
                    throw new InvalidArgumentException('At least two workflows match this subject. Set a different name on each and use the second (name) argument of this method.');
                }
                $matched = $workflow;
            }
        }

        if (!$matched) {
            throw new WorkflowNotFoundException(sprintf('Unable to find a workflow for class "%s".', get_class($subject)));
        }

        return $matched;
    }

    /**
     * @param string $checkedContentType
     * @param null|string $preferredWorkflowName
     *
     * @return bool
     */
    public function has($checkedContentType, $preferredWorkflowName = null)
    {
        foreach ($this->workflows as list($workflow, $supportedContentType)) {
            if ($this->supports($workflow, $supportedContentType, $checkedContentType, $preferredWorkflowName)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param \Symfony\Component\Workflow\Workflow $workflow
     * @param string $supportedContentType
     * @param string $checkedContentType
     * @param string $preferredWorkflowName
     *
     * @return bool
     */
    private function supports(Workflow $workflow, $supportedContentType, $checkedContentType, $preferredWorkflowName)
    {
        if ($checkedContentType !== $supportedContentType) {
            return false;
        }

        if (null === $preferredWorkflowName) {
            return true;
        }

        return $preferredWorkflowName === $workflow->getName();
    }

    /**
     * @param \eZ\Publish\API\Repository\Values\Content\Content $content
     *
     * @return string
     */
    private function getContentTypeIdentifier(Content $content)
    {
        $contentType = $this->contentTypeService->loadContentType($content->contentInfo->contentTypeId);

        return $contentType->identifier;
    }
}
