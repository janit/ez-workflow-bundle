<?php

namespace Inviqa\EzWorkflowBundle\EzBridge\ObjectState;

use eZ\Publish\API\Repository\ObjectStateService;
use eZ\Publish\API\Repository\Values\ObjectState\ObjectState;
use eZ\Publish\API\Repository\Values\ObjectState\ObjectStateGroup;
use Symfony\Component\Workflow\Workflow;

class ObjectStateProvider
{
    /** @var \eZ\Publish\API\Repository\ObjectStateService */
    private $objectStateService;

    /** @var null|array|ObjectStateGroup[] */
    private $inMemoryObjectStateGroups = null;

    /** @var array|ObjectState[] */
    private $inMemoryObjectStatesByGroupId = [];

    /**
     * @param \eZ\Publish\API\Repository\ObjectStateService $objectStateService
     */
    public function __construct(ObjectStateService $objectStateService)
    {
        $this->objectStateService = $objectStateService;
    }

    /**
     * @param Workflow $workflow
     *
     * @return \eZ\Publish\API\Repository\Values\ObjectState\ObjectStateGroup
     * @throws \Inviqa\EzWorkflowBundle\EzBridge\ObjectState\ObjectStateGroupNotFoundException
     */
    public function getObjectStateGroup(Workflow $workflow)
    {
        if (null === $this->inMemoryObjectStateGroups) {
            $existingObjectStateGroups = $this->objectStateService->loadObjectStateGroups();

            $this->inMemoryObjectStateGroups = [];
            foreach ($existingObjectStateGroups as $objectStateGroup) {
                $this->inMemoryObjectStateGroups[$objectStateGroup->identifier] = $objectStateGroup;
            }
        }

        if (isset($this->inMemoryObjectStateGroups[$workflow->getName()])) {
            return $this->inMemoryObjectStateGroups[$workflow->getName()];
        }

        throw new ObjectStateGroupNotFoundException(sprintf('No object state group was found in storage with name %s', $workflow->getName()));
    }

    /**
     * @param \eZ\Publish\API\Repository\Values\ObjectState\ObjectStateGroup $objectStateGroup
     * @param string $objectStateIdentifier
     *
     * @return \eZ\Publish\API\Repository\Values\ObjectState\ObjectState
     * @throws \Inviqa\EzWorkflowBundle\EzBridge\ObjectState\ObjectStateNotFoundException
     */
    public function getObjectState(ObjectStateGroup $objectStateGroup, $objectStateIdentifier)
    {
        if (!isset($this->inMemoryObjectStatesByGroupId[$objectStateGroup->id])) {
            $existingObjectStates = $this->objectStateService->loadObjectStates($objectStateGroup);

            $this->inMemoryObjectStatesByGroupId[$objectStateGroup->id] = [];
            foreach ($existingObjectStates as $objectState) {
                $this->inMemoryObjectStatesByGroupId[$objectStateGroup->id][$objectState->identifier] = $objectState;
            }
        }

        if (isset($this->inMemoryObjectStatesByGroupId[$objectStateGroup->id][$objectStateIdentifier])) {
            return $this->inMemoryObjectStatesByGroupId[$objectStateGroup->id][$objectStateIdentifier];
        }

        throw new ObjectStateNotFoundException(sprintf('No object state was found in storage with identifier %s in group %s',
                $objectStateIdentifier, $objectStateGroup->identifier
            )
        );
    }
}
