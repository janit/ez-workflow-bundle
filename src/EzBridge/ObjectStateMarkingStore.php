<?php

namespace Inviqa\EzWorkflowBundle\EzBridge;

use eZ\Publish\API\Repository\Repository;
use eZ\Publish\API\Repository\ObjectStateService;
use eZ\Publish\API\Repository\Values\Content\Content;
use Inviqa\EzWorkflowBundle\EzBridge\ObjectState\ObjectStateProvider;
use Symfony\Component\Workflow\Exception\InvalidArgumentException;
use Symfony\Component\Workflow\Marking;
use Symfony\Component\Workflow\MarkingStore\MarkingStoreInterface;

class ObjectStateMarkingStore implements MarkingStoreInterface
{
    /** @var \Inviqa\EzWorkflowBundle\EzBridge\ObjectState\ObjectStateProvider */
    private $objectStateProvider;

    /** @var \Inviqa\EzWorkflowBundle\EzBridge\WorkflowRegistry */
    private $registry;

    /** @var \eZ\Publish\API\Repository\ObjectStateService */
    private $objectStateService;

    /** @var \eZ\Publish\API\Repository\Repository */
    private $ezSignalSlotRepository;

    /**
     * @param \Inviqa\EzWorkflowBundle\EzBridge\ObjectState\ObjectStateProvider $objectStateProvider
     * @param \Inviqa\EzWorkflowBundle\EzBridge\WorkflowRegistry $registry
     * @param \eZ\Publish\API\Repository\Repository $ezSignalSlotRepository
     * @param \eZ\Publish\API\Repository\ObjectStateService $objectStateService
     */
    public function __construct(
        ObjectStateProvider $objectStateProvider,
        WorkflowRegistry $registry,
        Repository $ezSignalSlotRepository,
        ObjectStateService $objectStateService
    ) {
        $this->objectStateProvider = $objectStateProvider;
        $this->registry = $registry;
        if (!method_exists($ezSignalSlotRepository, 'sudo')) {
            throw new \InvalidArgumentException('Expecting a repository that supports sudo!');
        }
        $this->ezSignalSlotRepository = $ezSignalSlotRepository;
        $this->objectStateService = $objectStateService;
    }

    /**
     * Gets a Marking from a subject.
     *
     * @param Content $subject A subject
     *
     * @return Marking The marking
     */
    public function getMarking($subject)
    {
        $this->assertSubjectIsEzContent($subject);

        $objectStateGroup = $this->getObjectStateGroup($subject);

        $currentObjectState = $this->objectStateService->getContentState($subject->contentInfo, $objectStateGroup);
        if (!$currentObjectState) {
            return new Marking();
        }

        return new Marking(array($currentObjectState->identifier => 1));
    }

    /**
     * Sets a Marking to a subject.
     *
     * @param Content $subject A subject
     * @param Marking $marking A marking
     */
    public function setMarking($subject, Marking $marking)
    {
        $this->assertSubjectIsEzContent($subject);

        $objectStateGroup = $this->getObjectStateGroup($subject);

        $newObjectStateName = key($marking->getPlaces());
        $newObjectState = $this->objectStateProvider->getObjectState($objectStateGroup, $newObjectStateName);

        $currentObjectState = $this->objectStateService->getContentState($subject->contentInfo, $objectStateGroup);
        if ($currentObjectState->identifier !== $newObjectState->identifier) {
            $objectStateService = $this->objectStateService;
            // Using sudo as this method is called in Symfony Workflow class by default but the users might not have the permission
            // Permission if transition can be applied is done by our GuardListener class, so this should be safe.
            $this->ezSignalSlotRepository->sudo(
                function() use ($objectStateService, $subject, $objectStateGroup, $newObjectState)
                {
                    $objectStateService->setContentState($subject->contentInfo, $objectStateGroup, $newObjectState);
                }
            );
        }
    }

    /**
     * @param object $subject
     * @throws InvalidArgumentException
     */
    private function assertSubjectIsEzContent($subject)
    {
        if (!$subject instanceof Content) {
            throw new InvalidArgumentException('Only supporting ez content for this type of workflow marking store!');
        }
    }

    /**
     * @param $subject
     *
     * @return \eZ\Publish\API\Repository\Values\ObjectState\ObjectStateGroup
     * @throws \Inviqa\EzWorkflowBundle\EzBridge\ObjectState\ObjectStateGroupNotFoundException
     * @throws \Inviqa\EzWorkflowBundle\EzBridge\WorkflowNotFoundException
     */
    private function getObjectStateGroup($subject)
    {
        $workflow = $this->registry->get($subject);
        $objectStateGroup = $this->objectStateProvider->getObjectStateGroup($workflow);

        return $objectStateGroup;
    }
}
