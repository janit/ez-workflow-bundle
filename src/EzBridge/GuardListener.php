<?php

namespace Inviqa\EzWorkflowBundle\EzBridge;

use eZ\Publish\API\Repository\Values\Content\Content;
use eZ\Publish\Core\MVC\Symfony\Security\Authorization\Attribute as AuthorizationAttribute;
use Inviqa\EzWorkflowBundle\EzBridge\ObjectState\ObjectStateGroupNotFoundException;
use Inviqa\EzWorkflowBundle\EzBridge\ObjectState\ObjectStateNotFoundException;
use Inviqa\EzWorkflowBundle\EzBridge\ObjectState\ObjectStateProvider;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\AuthenticatedVoter;
use Symfony\Component\Workflow\Event\GuardEvent;
use Symfony\Component\Workflow\Exception\InvalidArgumentException;
use Symfony\Component\Workflow\Transition;

class GuardListener
{
    /** @var \Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface */
    private $authorizationChecker;

    /** @var \Inviqa\EzWorkflowBundle\EzBridge\WorkflowRegistry */
    private $workflowRegistry;

    /** @var \Inviqa\EzWorkflowBundle\EzBridge\ObjectState\ObjectStateProvider */
    private $objectStateProvider;

    /**
     * @param \Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface $authorizationChecker
     * @param \Inviqa\EzWorkflowBundle\EzBridge\WorkflowRegistry $workflowRegistry
     * @param \Inviqa\EzWorkflowBundle\EzBridge\ObjectState\ObjectStateProvider $objectStateProvider
     */
    public function __construct(
        AuthorizationCheckerInterface $authorizationChecker,
        WorkflowRegistry $workflowRegistry,
        ObjectStateProvider $objectStateProvider
    ) {
        $this->authorizationChecker = $authorizationChecker;
        $this->workflowRegistry = $workflowRegistry;
        $this->objectStateProvider = $objectStateProvider;
    }

    /**
     * @param \Symfony\Component\Workflow\Event\GuardEvent $event
     */
    public function onTransitionCheck(GuardEvent $event)
    {
        // We are only caring about ez content
        if (!($event->getSubject() instanceof Content)) {
            return;
        }

        // For all transitions, user should be logged in
        if (!$this->authorizationChecker->isGranted(AuthenticatedVoter::IS_AUTHENTICATED_FULLY)) {
            $event->setBlocked(true);
            return;
        }

        if (!$this->isAccessibleWorkflowTransition($event->getSubject(), $event->getTransition())) {
            $event->setBlocked(true);
        }
    }

    /**
     * @param \eZ\Publish\API\Repository\Values\Content\Content $content
     * @param \Symfony\Component\Workflow\Transition $transition
     *
     * @return bool
     */
    private function isAccessibleWorkflowTransition(Content $content, Transition $transition)
    {
        // No permission, if content type is not assigned to a workflow
        try {
            $workflow = $this->workflowRegistry->get($content);
        } catch (WorkflowNotFoundException $ex) {
            return false;
        }

        // No permission, if user is not allowed to edit ez content (in it's current state).
        if (!$this->authorizationChecker->isGranted(new AuthorizationAttribute('content', 'edit', ['valueObject' => $content]))) {
            return false;
        }

        // Edge case: No permission, if related ez ObjectStateGroup can not be found (wrong data setup)
        try {
            $objectStateGroup = $this->objectStateProvider->getObjectStateGroup($workflow);
        } catch (ObjectStateGroupNotFoundException $ex) {
            return false;
        }

        // Edge case: No permission, if related ez ObjectState can not be found (wrong data setup)
        $targetStateName = $this->getStateNameFromTransitionPlaces($transition->getTos());
        try {
            $targetObjectState = $this->objectStateProvider->getObjectState($objectStateGroup, $targetStateName);
        } catch (ObjectStateNotFoundException $ex) {
            return false;
        }

        // No permission, if user has not ez permission to set content's state to the transition target.
        // Note: currently ez does not provide a limitation of the transition source...
        // $currentStateName = $this->getStateName($transition->getFroms()); // of no value at the moment, as there is no "OldState" limitation...
        if (!$this->authorizationChecker->isGranted(new AuthorizationAttribute('state', 'assign', ['valueObject' => $content, 'targets' => $targetObjectState]))) {
            return false;
        }

        return true;
    }

    /**
     * @param array|string[] $transitionPlaces
     *
     * @return string
     */
    private function getStateNameFromTransitionPlaces(array $transitionPlaces)
    {
        if (count($transitionPlaces) !== 1) {
            throw new InvalidArgumentException('Only transitions from *one* place to *one* other are currently supported!');
        }

        return $transitionPlaces[0];
    }
}
