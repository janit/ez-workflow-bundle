YUI.add('workflow-actionbar-plugin', function (Y) {
    'use strict';
    /**
     * @module workflow-actionbar-plugin
     */

    Y.namespace('EzWorkflow.Plugin');

    Y.EzWorkflow.Plugin.ActionBarPlugin = Y.Base.create('actionBarPlugin', Y.eZ.Plugin.ViewServiceBase, [], {
        initializer: function () {
            var view = this.get('host'),
                content = view.get('content'),
                content_id = content.get('contentId');

            this._checkContentWorkflow(content_id);
        },

        /**
         * Checking if current content has a workflow assigned and if so:
         * - display button showing content state as a disabled button using it's label
         * - display button(s) for all available workflow transitions
         *
         * @param contentId
         * @private
         */
        _checkContentWorkflow: function (contentId) {
            var that = this,
                contentWorkflowUrl = this.get('contentWorkflowUrl').replace('%d', contentId);

            Y.io(contentWorkflowUrl, {
                method: 'GET',
                on: {
                    success: this._onWorkflowTransitionsResponseSuccess.bind(that),
                    failure: this._onWorkflowTransitionsResponseFailure.bind(that)
                }
            }, that);
        },

        /**
         * Event handler to handle success of workflow transitions ajax call.
         *
         * @param index
         * @param response
         * @private
         */
        _onWorkflowTransitionsResponseSuccess: function(index, response) {
            var parsedResponse = Y.JSON.parse(response.response);

            if (parsedResponse.workflow != null) {
                var view = this.get('host');

                // just displaying current status as a "label" (button disabled)
                view.addAction(
                    new Y.eZ.ButtonActionView({
                        actionId: "workflow_state",
                        disabled: true,
                        label: "State: " + parsedResponse.state,
                        priority: 500
                    })
                );

                for (var transitionName in parsedResponse.transitions) {
                    view.addAction(
                        new Y.EzWorkflow.StateChangeActionView({
                            actionId: transitionName,
                            disabled: false,
                            label: parsedResponse.transitions[transitionName],
                            priority: 300
                        })
                    );
                }

                view.render();
            }
        },

        /**
         * Event handler to handle failure of workflow transitions ajax call.
         *
         * @param index
         * @param response
         * @private
         */
        _onWorkflowTransitionsResponseFailure: function(index, response) {
            this._notifyError(this.get('host').get('content').get('contentId'));
            return;
        },

        /**
         * Notifies the editor about a publishing error
         *
         * @method _notifyError
         * @protected
         * @param {String} contentId
         */
        _notifyError: function (contentId) {
            this.get('host').fire('notify', {
                notification: {
                    identifier: 'workflow-transitions-' + contentId + '-' + this.get('host').get('languageCode'),
                    text: 'An error occurred while fetching workflow information for content',
                    state: 'error',
                    timeout: 0,
                },
            });
        },
    }, {
        NS: 'ezWorkflowEditActionBarPlugin',
        ATTRS: {
            /**
             * Content which is currently loaded in content edit view
             *
             * @attribute content
             * @type eZ.Content
             * @default {}
             * @required
             */
            content: {
                value: {}
            },

            /**
             * The URL string to AJAX endpoint to fetch a content item's attached workflow, status and available transitions.
             * The URL string contains a placeholder, where content's ID is placed (%d).
             *
             * @attribute contentWorkflowUrl
             * @type String
             * @default 'ez-workflow/transitions/%d'
             * @readOnly
             */
            contentWorkflowUrl: {
                value: 'ez-workflow/transitions/%d',
                readOnly: true
            },
        }
    });

    Y.eZ.PluginRegistry.registerPlugin(
        Y.EzWorkflow.Plugin.ActionBarPlugin, ['actionBarView']
    );
});
