YUI.add('workflow-editactionbar-plugin', function (Y) {
    'use strict';
    /**
     * @module workflow-editactionbar-plugin
     */

    Y.namespace('EzWorkflow.Plugin');

    Y.EzWorkflow.Plugin.EditActionBarPlugin = Y.Base.create('editActionBarPlugin', Y.eZ.Plugin.ViewServiceBase, [], {
        initializer: function () {
            var view = this.get('host'),
                content = view.get('content'),
                content_id = content.get('contentId'),
                content_type = view.get('contentType').get('identifier');

            this._checkContentTypeWorkflowSupport(content_type, content_id);
        },

        /**
         * Checking if current content type has a supported workflow if so:
         * - display button showing content state as a disabled button using it's label (if present, *not* present on content creation)
         * - hide save button and change "publish" button label
         *
         * @param contentTypeIdentifier
         * @private
         */
        _checkContentTypeWorkflowSupport: function (contentTypeIdentifier, contentId) {
            var checkWorkflowUrl = this.get('checkWorkflowSupportUrl').replace('%s', contentTypeIdentifier);
            if (contentId) {
                checkWorkflowUrl = this.get('contentWorkflowUrl').replace('%d', contentId);
            }

            var that = this;
            Y.io(checkWorkflowUrl, {
                method: 'GET',
                on: {
                    success: this._onWorkflowCheckResponseSuccess.bind(that),
                    failure: this._onWorkflowCheckResponseFailure.bind(that)
                }
            }, that);
        },

        /**
         * Event handler to handle success of workflow transitions ajax call.
         *
         * @param index
         * @param response
         * @private
         */
        _onWorkflowCheckResponseSuccess: function(index, response) {
            var parsedResponse = Y.JSON.parse(response.response);
            if (parsedResponse.has_workflow != false) {
                var view = this.get('host');

                // removing default save and publish button
                view.removeAction('save');
                view.removeAction('publish');

                if (parsedResponse.state) {
                    // display current status as a "label" (button disabled)
                    view.addAction(
                        new Y.eZ.ButtonActionView({
                            actionId: "workflow_state",
                            disabled: true,
                            label: "State: " + parsedResponse.state,
                            priority: 500
                        })
                    );
                }

                // use "publish" for all saving actions on content under editorial workflow
                view.addAction(
                    new Y.eZ.ButtonActionView({
                        actionId: "publish",
                        disabled: false,
                        label: "Save changes",
                        priority: 200
                    })
                );

                view.render();
            }
        },

        /**
         * Event handler to handle failure of workflow transitions ajax call.
         *
         * @param index
         * @param response
         * @private
         */
        _onWorkflowCheckResponseFailure: function(index, response) {
            this._notifyError(this.get('host').get('contentType').get('identifier'));
            return;
        },

        /**
         * Notifies the editor about a publishing error
         *
         * @method _notifyError
         * @protected
         * @param {String} contentType
         */
        _notifyError: function (contentType) {
            this.get('host').fire('notify', {
                notification: {
                    identifier: 'workflow-support-' + contentType + '-' + this.get('host').get('languageCode'),
                    text: 'An error occurred while fetching workflow support information for content type',
                    state: 'error',
                    timeout: 0,
                },
            });
        },
    }, {
        NS: 'ezWorkflowEditActionBarPlugin',
        ATTRS: {
            /**
             * Content which is currently loaded in content edit view
             *
             * @attribute content
             * @type eZ.Content
             * @default {}
             * @required
             */
            content: {
                value: {}
            },

            /**
             * The URL string to AJAX endpoint to check a content type is supported by any configured workflow.
             * The URL string contains a placeholder, where content type identifiers's string is placed (%s).
             *
             * @attribute checkWorkflowSupportUrl
             * @type String
             * @default 'ez-workflow/supported/%s'
             * @readOnly
             */
            checkWorkflowSupportUrl: {
                value: 'ez-workflow/supported/%s',
                readOnly: true
            },

            /**
             * The URL string to AJAX endpoint to fetch a content item's attached workflow, status and available transitions.
             * The URL string contains a placeholder, where content's ID is placed (%d).
             *
             * @attribute contentWorkflowUrl
             * @type String
             * @default 'ez-workflow/transitions/%d'
             * @readOnly
             */
            contentWorkflowUrl: {
                value: 'ez-workflow/transitions/%d',
                readOnly: true
            },
        }
    });

    Y.eZ.PluginRegistry.registerPlugin(
        Y.EzWorkflow.Plugin.EditActionBarPlugin, ['editActionBarView']
    );
});
