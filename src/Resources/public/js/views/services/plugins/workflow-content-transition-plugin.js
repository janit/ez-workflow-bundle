YUI.add('workflow-content-transition-plugin', function (Y) {
    "use strict";
    /**
     * Provides the promote article plugin
     *
     * @module workflow-content-transition-plugin
     */
    Y.namespace('EzWorkflow.Plugin');

    /**
     * Plugin listening to "transitionAction" events and applying content transition via API call.
     *
     * @namespace EzWorkflow.Plugin
     * @class PromoteArticle
     * @constructor
     * @extends eZ.Plugin.ViewServiceBase
     */
    Y.EzWorkflow.Plugin.ContentTransition = Y.Base.create('contentTransitionPlugin', Y.eZ.Plugin.ViewServiceBase, [], {
        initializer: function () {
            this.onHostEvent('*:transitionAction', this._executeTransition);

            var service = this.get('host');

            service.after('transitionApplied', Y.bind(this._redirectAfterTransition, this));
        },

        _executeTransition: function(e) {
            var service = this.get('host'),
                app = service.get('app');

            var content = service.get('content'),
                contentId = content.get('contentId'),
                transition = e.transition;

            var contentTransitionUrl = this.get('contentTransitionUrl').replace('%d', contentId).replace('%s', transition);

            var that = this;
            Y.io(contentTransitionUrl, {
                method: 'POST',
                on: {
                    success: this._onWorkflowTransitionSuccess.bind(that),
                    failure: this._onWorkflowTransitionFailure.bind(that)
                }
            }, that);
        },

        /**
         * Event handler to go through the tasks after the content transition was
         * successfully applied to the next state.
         *
         * @param index
         * @param response
         * @private
         */
        _onWorkflowTransitionSuccess: function(index, response) {
            var service = this.get('host'),
                app = service.get('app'),
                content = service.get('content'),
                notificationIdentifier = this._buildNotificationIdentifier(content.get('id'));

            if (response.status === 204) {
                service.fire('notify', {
                    notification: {
                        identifier: notificationIdentifier,
                        text: 'Content state has been changed',
                        state: 'done',
                        timeout: 5,
                    },
                });

                content.load({api: service.get('capi')}, function (error, response) {
                    /**
                     * Fired when the content transition was applied
                     *
                     * @event transitionApplied
                     * @param {eZ.Content} content
                     */
                    service.fire('transitionApplied', {content: content});
                });
            } else {
                this._notifyError(content.get('contentId'));
                app.set('loading', false);
                return;
            }
        },

        /**
         * Event handler to handle failure of promote workflow ajax call.
         *
         * @param index
         * @param response
         * @private
         */
        _onWorkflowTransitionFailure: function(index, response) {
            this._notifyError(this.get('host').get('content').get('contentId'));
            this.get('host').get('app').set('loading', false);
            return;
        },

        /**
         * Notifies the editor about a promotion error
         *
         * @method _notifyError
         * @protected
         * @param {String} contentId
         */
        _notifyError: function (contentId) {
            this.get('host').fire('notify', {
                notification: {
                    identifier: this._buildNotificationIdentifier(contentId),
                    text: 'An error occurred while changing content state',
                    state: 'error',
                    timeout: 0,
                },
            });
        },

        /**
         * Builds the notification identifier for the transition notification
         *
         * @method _buildNotificationIdentifier
         * @param {eZ.Content} contentId
         * @protected
         */
        _buildNotificationIdentifier: function (contentId) {

            if (contentId) {
                return 'workflow-transition-' + contentId + '-' + this.get('host').get('languageCode');
            } else {
                return 'workflow-transition-' + this.get('host').get('languageCode');
            }
        },

        _redirectAfterTransition: function(e) {
            this.get('host').fire('refreshView');
        }
    }, {
        NS: 'workflowTransition',

        ATTRS: {
            /**
             * The URL string to AJAX endpoint to apply a content transition.
             * The URL string contains a placeholder, where content's ID is placed (%d) and one for transition name (%s).
             *
             * @attribute contentTransitionUrl
             * @type String
             * @default 'ez-workflow/transitions/%d/%s'
             * @readOnly
             */
            contentTransitionUrl: {
                value: 'ez-workflow/transitions/%d/%s',
                readOnly: true
            },
        },
    });

    Y.eZ.PluginRegistry.registerPlugin(
        Y.EzWorkflow.Plugin.ContentTransition, ['locationViewViewService']
    );
});
