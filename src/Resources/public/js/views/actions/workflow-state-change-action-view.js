YUI.add('workflow-state-change-action-view', function (Y) {
    'use strict';

    /**
     * Provides the editorial workflow state change action view class
     *
     * @module workflow-state-change-action-view
     */
    Y.namespace('EzWorkflow');

    /**
     * Create Publish Button Action View
     *
     * @namespace EzWorkflow
     * @class StateChangeActionView
     * @constructor
     * @extends eZ.ButtonActionView
     */
    Y.EzWorkflow.StateChangeActionView = Y.Base.create('stateChangeActionView', Y.eZ.ButtonActionView, [], {
        events: {
            '.action-trigger': {
                'tap': '_handleActionClick'
            }
        },

        initializer: function() {
        },

        /**
         * Override an inherited method to force the correct button template.
         */
        _generateViewClassName: function (name) {
            return Y.eZ.TemplateBasedView.VIEW_PREFIX + 'buttonactionview';
        },

        /**
         * Handles tap on the view's action button
         *
         * @method _handleActionClick
         * @param e {Object} event facade
         * @protected
         */
        _handleActionClick: function(e) {
            e.preventDefault();
            if ( !this.get('disabled') ) {
                this._showWarnDialog(function () {
                    this.fire('transitionAction', {
                        content: this.get('content'),
                        transition: this.get('actionId')
                    });
                });
            }
        },

        _showWarnDialog: function(callable) {
            this.fire('confirmBoxOpen', {
                config: {
                    title: 'Are you sure you want to change state of this content now?',
                    confirmHandler: Y.bind(callable, this)
                },
            });
        },
    },{
        ATTRS: {
        }
    });
});
